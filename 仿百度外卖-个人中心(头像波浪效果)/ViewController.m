//
//  ViewController.m
//  仿百度外卖-个人中心(头像波浪效果)
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ViewController.h"
#import "WavesView.h"


@interface ViewController ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) WavesView *waves;

@end

@implementation ViewController

- (UIImageView *)iconImageView{
    
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.waves.frame.size.width/2 - 30, 0, 60, 60)];
        _iconImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        _iconImageView.layer.borderWidth = 2;
        _iconImageView.layer.cornerRadius = 30;
    }
    return _iconImageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self settingContent];
}


- (void)settingContent{
    //创建对线
    self.waves = [[WavesView alloc]initWithFrame:CGRectMake(0, 22, self.view.frame.size.width, 150)];
    
    self.waves.backgroundColor =[UIColor redColor];
    
    [self.view addSubview:self.waves];
    
    [self.waves addSubview:self.iconImageView];
    
    //这里把头像的 frame 传入到 wavesView里,让其通过 frame来计算该变量
    self.waves.imageFrame = _iconImageView.frame;
    //防止循环引用
    __weak typeof(self)weakSelf = self;
    
    self.waves.waveBlock = ^(CGRect imageFrame){
        //修改头像view的frame(时时的通过 block 回调改变的 frame)
        weakSelf.iconImageView.frame = imageFrame;
        
    };
    //开始执行
    [self.waves startWaveAnimation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
